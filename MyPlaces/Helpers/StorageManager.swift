//
//  StorageManager.swift
//  MyPlaces
//
//  Created by OzOn on 06.01.2020.
//  Copyright © 2020 Mr.OzOn. All rights reserved.
//

import RealmSwift

let realm = try! Realm()

class StorageManage {
    
    static func saveObject(_ place: Place) {
        try! realm.write {
            realm.add(place)
        }
    }
    
    static func deleteObject(_ place: Place) {
        try! realm.write {
            realm.delete(place)
        }
    }
}
